The purpose of this cartridge is to allow the Storefront Toolkit to work with the Commerce Center.  
Just upload this cartridge to your site and add it to the beginning of your cartridge path.  
The templates in this cartridge were copied from the SiteGenesis 12.5 cartridges and were only modified 
to change or enhance the dwMarker tags.  Also included is a new javascript file (CCdwux-12.5.js) 
that changes the behavior of the urls needed to call the Commerce Center instead 
of the Business Manager.

In order to have the Storefront Toolkit work with
the Business Manager just remove this cartridge from the cartridge list.

1) JAVASCRIPT

The following include was added to components/header/htmlhead_UI.isml

<script src="${URLUtils.staticURL('/js/CCdwux-12.5.js')}" type="text/javascript"></script>


2) CONTENT ASSET TEMPLATE

The following was added to the dwMarker="content" tag in content/content/htmlcontentasset.isml

dwContentAssetID="${pdict.Content.ID}" dwFolderID="${pdict.Content.folders.iterator().next().ID}"


3) PRODUCT TEMPLATES

Changed all dwMarker="product" tags

From
	dwContentID="${Product.UUID}"  
To
	dwContentID="${Product.ID}"

product/productsearchhittile.isml
product/producttile.isml
product/producttopcontent.isml
product/producttopcontentPS.isml