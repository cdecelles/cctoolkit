// Closure for pagemarker functionality
(function() {
	Ext.namespace('dw.__ux.pm');

	// the current highlighted element
	var dwActiveElement;
	
	// the current element used to do the highlighting
	var dwActiveHighlight;

	// the old background of the active element
	//var dwOldBackground;

	// the marker div element
	var dwToolTip;

	// the color we use for highlighting
	var highlightColor = "#ffa500";

	// marker mode: 0 - page information, 1 - content information
	var dwMode = 0;

	//
	var organizationAndSiteName ="";
	
	var baseEditURL = "";
	
	/*
	Stuff for browser compatibility.
	*/
	var is_ie = (navigator.userAgent.toLowerCase().indexOf( "msie" ) != -1);

	function ietruebody()
	{
		return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
	}


	/*
	This function is called from the bookmarklet to toogle the markers
	for content on.
	*/
	function showContentInformation( color )
	{
		if ( color == null )
		{
			color = "#90ee90";
		}
		dwInitMarkers( color, 1 );
	}


	/*
	This function is called from the bookmarklet to toogle the markers
	on and off.
	*/
	function showPageInformation( color )
	{
		if ( color == null )
		{
			color = "#ffa500";
		}
		dwInitMarkers( color, 0 );
	}


	/*
	This function is called to initialize the marker.
	*/
	function dwInitMarkers( color, mode )
	{
		// remove existing handlers
		dwRemoveHandler();

		// set the mode
		dwMode = mode;

		// set the highlighting color
		highlightColor = (color != null) ? color : "#ffa500";

		// set the key handler
		//document.onkeydown = dwOnKeyDown;
		
		organizationAndSiteName= dwGetOrganizationAndSiteName();
		
		baseEditURL = dwGetBaseEditURL();
		
		// initialize the tooltip div
		dwToolTip = document.getElementById( "dwToolTip" );
		if ( dwToolTip == null )
		{
			dwToolTip = document.createElement( "div" );
			with( dwToolTip.style )
			{
				position = 'absolute';
				visibility = 'visible';
				zIndex = '9999';
			}
			dwToolTip.id = "dwpagemarkertooltip";
			document.body.appendChild( dwToolTip );	
		}
		
		// add all event handlers
		dwAddHandler();	
		dwToolTip.onmouseover = dwOnMouseOverToolTip;
		dwToolTip.onmouseout = dwOnMouseOutToolTip;
	}

	// Hides any page markers that are currently shown
	dw.__ux.pm.hidePageMarkers = function() {
		if ( dwActiveElement != null )
		{
			//dwActiveElement.style.backgroundColor = dwOldBackground.color;
			//dwActiveElement.style.backgroundImage = dwOldBackground.image;
			dw.__ux.unhighlight(dwActiveElement);
			dwActiveElement = null;
		}
		
		if ( dwToolTip != null )
		{
			dwToolTip.style.visibility = 'hidden';
		}
		
		dwRemoveHandler();
	}
	
	/*
	This function handles pressing the ESC key.
	*/
	function dwOnKeyDown( event )
	{
		key = document.all ? window.event.keyCode : event.keyCode;
		if ( key == 27 )
		{
			dw.__ux.pm.hidePageMarkers();
		}
	}


	/*
	This function walks through all elements and adds the mouseover
	handler, which shows the tool tip.
	*/
	function dwAddHandler()
	{
		Ext.each(Ext.query("A, FORM, BODY, DIV, TD, TH, SPAN, TABLE"), function(el) {
			if (dwCheckMarkerInfo(el)) {
				el.onmouseover = dwOnMouseOverMarker;
				el.onmouseout = dwOnMouseOutMarker;
				el.onmousemove = dwOnMouseMoveMarker;
			}
		});
	}

	/*
	This function walks through all elements and removes the
	mouseover handlers.
	*/
	function dwRemoveHandler()
	{
		Ext.each(Ext.query("A, FORM, BODY, DIV, TD, TH, SPAN, TABLE"), function(el) {
			if (el.onmouseover == dwOnMouseOverMarker) {
				el.onmouseover = null;
				el.onmouseout = null;
				el.onmousemove = null;
			}
		});
	}


	/*
	This function checks, whether an element has a information. If the element is an
	'a' or 'form' it can have directly following information. For all other elements it
	is checked, whether a previous silbing is a marker comment.
	*/
	function dwCheckMarkerInfo( element )
	{
		// we are in page information mode 
		if ( dwMode == 0 )
		{
			// 'a' or 'form' so check for following comment
			if ( element.tagName == "A" ||
				 element.tagName == "FORM" )
			{
				var child = element.firstChild;
			
				// check for a comment immediately following
				if ( child != null && child.nodeType == 8 )
				{
					var type = dwGetCommentAttribute( child.nodeValue, "dwMarker" );
					if ( type == "link" || type == "form" )
					{
						return true;
					}
				}
			}
			
			// for all other elements search for previous sibling comment
			else
			{	
				prev = element.previousSibling;
				while( prev != null && (prev.nodeType == 3 || prev.nodeType == 8) )
				{
					if ( prev.nodeType == 8 )
					{
						var type = dwGetCommentAttribute( prev.nodeValue, "dwMarker" );
						if ( type == "linclude" || type == "rinclude" || type == "page" || type == "decorator")
						{
							return true;
						}
					}
					prev = prev.previousSibling;
				}
			}
		}
		
		// we are in content information mode
		else
		{
			return dwGetContentMessage( element ) != '';
		}		
		return false;
	}


	/*
	This function takes an element and checks, whether a marker comment
	is the first child. It than calculates the message string and returns
	it. If no information is associated null is returned.
	*/
	function dwGetMarkerInfo( element )
	{
		// are we in page information mode
		if ( dwMode == 0 )
		{
			// 'a' or 'form' so check for following comment
			if ( element.tagName == "A" ||
				 element.tagName == "FORM" )
			{
				var child = element.firstChild;
				if ( child != null && child.nodeType == 8 )
				{
					// get the marker type
					var type = dwGetCommentAttribute( child.nodeValue, "dwMarker" );
					if ( type != null && (type == "link" || type == "form") )
					{
						// get the attributes of the marker
						var pipelineTitle = dwGetCommentAttribute( child.nodeValue, "dwPipelineTitle" );
						var pipelineURL = dwGetCommentAttribute( child.nodeValue, "dwPipelineURL" );
		
						// generate the message
						var message = "<b>";			
						message += (type == "link") ? "Link" : "Form";
						message += "</b><br>";
						message += "Open Pipeline <span class='dwstudio-link' onclick='dwstudio(this,\""
							+ pipelineURL
							+ "\");'>"
							+ pipelineTitle
							+ "</span>";
					
						return message;
					}
				}
			}
			
			// collect all info comments up in the hierachie
			else
			{
				var indent = "";
				var message = "";
				while( element != null )
				{
					if ( element.nodeType == 8 )
					{
						// get the marker type
						var type = dwGetCommentAttribute( element.nodeValue, "dwMarker" );
						if ( type != null )
						{
							// get the attributes of the marker
							var pipelineTitle = dwGetCommentAttribute( element.nodeValue, "dwPipelineTitle" );
							var pipelineURL = dwGetCommentAttribute( element.nodeValue, "dwPipelineURL" );
							var templateTitle = dwGetCommentAttribute( element.nodeValue, "dwTemplateTitle" );
							var templateURL = dwGetCommentAttribute( element.nodeValue, "dwTemplateURL" );
				
							// generate the title
							message += "<b>" + indent + " ";			
							if ( type == "rinclude" )
							{
								message += "Remote Include";
							}
							else if ( type == "linclude" )
							{
								message += "Local Include";
							}
							else if ( type == "decorator" )
							{
								message += "Decorator";
							}						
							else if ( type == "page" )
							{
								message += "Page";
							}
							else if ( type == "slot" )
							{
								message += "Slot";
							}
							else
							{
								message += "???";
							}
							message += "</b><br>";
							
							// generate the message and links
							if ( templateURL != null )
							{
								message += "Open Template <span class='dwstudio-link' onclick='dwstudio(this,\""
										+ templateURL
										+ "\");'>"
										+ templateTitle + "</span><br>";
							}
							if ( pipelineURL != null )
							{
								message += "Open Pipeline <span class='dwstudio-link' onclick='dwstudio(this,\""
										+ pipelineURL
										+ "\");'>"
										+ pipelineTitle + "</span><br>";
							}
						}
					}
						
					// walk up the DOM tree
					var next = element.previousSibling;
					if ( next == null )
					{
						indent += "#";
						next = dwGetParentNode(element);
					}
					element = next;
				}
				return message;
			}
		}
		
		// we are in content information mode
		else
		{
			return "<b>Content</b>" + dwGetContentMessage( element );
		}
		return null;
	}

	/*
	This function returns the overall content message for the given element, checking up the hierarchy.
	*/
	function dwGetContentMessage( element )
	{
		var firstChildFunction = new Function('element', 'return element.firstChild');
		var nextSiblingFunction = new Function('element', 'return element.nextSibling');
		var previousSiblingFunction = new Function('element', 'return element.previousSibling');

		var message = '';
		
		contentMessage = dwGetContentInfo( element, firstChildFunction, nextSiblingFunction); 
		if ( contentMessage != null )
		{
			message = message + "<br/>" + contentMessage;
		}
			
		while ( element != null )
		{
			contentMessage = dwGetContentInfo( element, previousSiblingFunction, previousSiblingFunction );
			if ( contentMessage != null )
			{
				message = message + "<br/>" + contentMessage;
			}
			
			element = dwGetParentNode(element);
		}
		
		return message;
	}

	/*
	This function returns the content message for the given element, checking elements produced by the given functions.
	*/
	function dwGetContentInfo( element, getFirstFunction, getNextFunction )
	{
		// content must be embedded in a DIV
		if ( element.tagName == "DIV" || element.tagName == "TD" || element.tagName == "TH" )
		{
			// loop through all children and check for a content comment
			var child = getFirstFunction( element );

			while( child != null && (child.nodeType == 3 || child.nodeType == 8) )
			{
				if ( child.nodeType == 8 )
				{
					var type = dwGetCommentAttribute( child.nodeValue, "dwMarker" );
					if ( type == "product" || type == "content" || type == "slot" )
					{

						// get the attributes of the marker
						var contentID = dwGetCommentAttribute( child.nodeValue, "dwContentID" );
						var url=baseEditURL;

						// if organizationAndSiteName =="" the host name is used but this 
						// might prevent the pagemarker from getting displayed correctly
						// but is the correct rewrite according to the rewrite rules
						
                        var host = window.location.hostname;
	             		if ( organizationAndSiteName == ""  )
	             		{
	             			organizationAndSiteName= host;
	             		}

						if( url.match(",pd.html")!= null )
						{
				              var regex = new RegExp( "([^/]+),([^/]+),pd.html(;.*)?", "" );   
					       	  url=url.replace(regex,"http://"+host+"/on/demandware.store/"+organizationAndSiteName+"/$2/Product-Show?pid=$1");
					     
				        }else if(url.match(",cp.html")!= null ){
						 	  var regex = new RegExp( "([^/]+),([^/]+),([^/]+),cp.html(;.*)?", "" );   
					     	  url=url.replace(regex,"http://"+host+"/on/demandware.store/"+organizationAndSiteName+"/$3/Product-ShowInCategory?cgid=$1}&pid=$2");
					    
						}else if( url.match(",sc.html")!= null){
					          var regex = new RegExp( "([^/]+),([^/]+),sc.html(;.*)?", "" );   
					          url=url.replace(regex,"http://"+host+"/on/demandware.store/"+organizationAndSiteName+"/$2/Search-Show?cgid=$1");
					    
						}else if( url.match(",sp.html")!= null ){
						       var regex = new RegExp( "([^/]+),([^/]+),sp.html(;.*)?", "" );   
					          url=url.replace(regex,"http://"+host+"/on/demandware.store/"+organizationAndSiteName+"/$2/Search-Show?pid=$1");
					    
						}else if( url.match(",pg.html")!= null ){
					    	  var regex = new RegExp( "([^/]+),([^/]+),pg.html(;.*)?", "" );   
					          url=url.replace(regex,"http://"+host+"/on/demandware.store/"+organizationAndSiteName+"/$2/Page-Show?cid=$1");  
						}
				     
					   //clean out rewritten URL
					   url= url.substring(url.lastIndexOf("http://"),url.length);
					   // delete "forgotten" placeholders from replacement
					 
					   // make sure that no more than one ? is in URL
	                      while(url.indexOf("?")<url.lastIndexOf("?"))
	                       {
	                   		var suburl = url.substring(0,url.lastIndexOf("?"));
	                   		var end = url.substring(url.lastIndexOf("?")+1);
	                   		url = suburl+"&"+end;    
	                   	}
						   
						// generate the URL for the product editor
						var segs = url.split( "/" );	
						var prdURL = "https://" + segs[2] + "/" + segs[3] + "/" +
								segs[4] + "/";
								
						var dash = segs[5].indexOf( "-" );	
						var dashSite = segs[5].search( "-Site$" );
						
						if ( (dash > 0) && (dashSite>0))
						{
							if ( type == "product" )
							{									
								var prdURL = "https://" + segs[2] + "/s/-/dw/cc_app/#products/edit?sku=" + contentID + "&locale=default&editor=details&offset=0&q=";
								
								// generate the message
								return "Edit <a href=\"\" onclick=\"window.open('" + prdURL +
									"', 'Commerce Center'); return false;\">Product</a> in Commerce Center.";
							}
							else if ( type == "content" )
							{
								
								var cid = dwGetCommentAttribute( child.nodeValue, "dwContentAssetID" );
								var fid = dwGetCommentAttribute( child.nodeValue, "dwFolderID" );
								
								var contentURL = "https://" + segs[2] + "/s/-/dw/cc_app/#content/edit?site=" + segs[5].substring( dash+1, dashSite ) + "&editor=content&state=edit&locale=default&folder=" + fid + "&content=" + cid;
								
								// generate the message
								return "Edit <a href=\"\" onclick=\"window.open('" + contentURL +
									"', 'Commerce Center'); return false;\">Content</a> in Commerce Center.";					
							}
							else if ( type == "slot" )
							{
								var context = dwGetCommentAttribute( child.nodeValue, "dwContext" );
								var contextID = dwGetCommentAttribute( child.nodeValue, "dwContextID" );
								var contentID = dwGetCommentAttribute( child.nodeValue, "dwContentID" );
								
								var parentID = "";
								
								if ( context == "global" )
								{
									parentID = segs[5].substring( dash+1, dashSite );
								}
								else
								{
									parentID = contextID;
								}
									
								slotURL = "https://" + segs[2] + "/s/-/dw/cc_app/#slots/edit?site=" + segs[5].substring( dash+1, dashSite ) + "&editor=slotConfigurations&locale=default&slotType=" + context.toUpperCase() + "&slotId=" + contentID + "&parentId=" + parentID;
																		
								// generate the message
								return "Edit <a href=\"\" onclick=\"window.open('" + slotURL +
									"', 'Commerce Center' ); return false;\">Content Slot</a> in Commerce Center.";				
							}
					
						}
						else
						{
							return null;
						}							
					}
				}
				child = getNextFunction( child );
			}
		}		
		
		return null;
	}

	/*
	Helper function to parse an attribute from an information comment.
	*/
	function dwGetCommentAttribute( comment, name )
	{
		var regex = new RegExp( name + "=\"([^\"]*)\"", "" );
		var matches = comment.match( regex );
		if ( matches != null && matches.length > 1 )
		{
			return matches[1];
		}
		return null;
	}


	/*
	This is our mouse over handler to show the tooltip
	*/
	function dwOnMouseOverMarker( e )
	{
		var to = is_ie ? event.toElement : e.target;

		// check that we are the first marker element to receive the event
		if ( this == dwGetFirstMarkerHighlightOrToolTip( to ) && this != dwActiveElement && this != dwActiveHighlight )
		{
			var highlight = dw.__ux.highlight({el:this, color:highlightColor, opacity:0.25});
			if (!highlight) {
				return;
			}
			
			dwActiveElement = this;
			//dwOldBackground = {
			//        color: this.style.backgroundColor,
			//        image: this.style.backgroundImage
			//};
			//this.style.backgroundColor = highlightColor;
			
			Ext.each(Ext.get(highlight.id), function(el) {
				dwActiveHighlight = el.dom;
				dwActiveHighlight.onmouseover = dwOnMouseOverMarker;
				dwActiveHighlight.onmouseout = dwOnMouseOutMarker;
				dwActiveHighlight.onmousemove = dwOnMouseMoveMarker;
			});
			
			if ( dwToolTip.style.visibility != 'visible' )
			{ 
				dwToolTip.innerHTML = dwGetMarkerInfo( this );
				with( dwToolTip.style )
				{
					visibility = 'visible';
					if ( is_ie )
					{
						left = event.clientX + ietruebody().scrollLeft + 1 + 'px';
						top = event.clientY + ietruebody().scrollTop + 'px';
					}
					else
					{
						left = e.clientX + window.pageXOffset + 1 + 'px';
						top = e.clientY + window.pageYOffset + 'px';
					}										
				}
			}
		}
	}


	/*
	The function handles a mouse move over an active element. It move the
	tooltip to follow the mouse.
	*/
	function dwOnMouseMoveMarker( e )
	{
		var src = is_ie ? event.srcElement : e.target;

		// check that we are the first marker element to receive the event
		if ( this == dwGetFirstMarkerHighlightOrToolTip( src ) )
		{
			if ( dwActiveElement != null && (this == dwActiveElement || this == dwActiveHighlight))
			{
				with( dwToolTip.style )
				{
					visibility = 'visible';
					if ( is_ie )
					{
						left = event.clientX + ietruebody().scrollLeft + 1 + 'px';
						top = event.clientY + ietruebody().scrollTop + 'px';
					}
					else
					{
						left = e.clientX + window.pageXOffset + 1 + 'px';
						top = e.clientY + window.pageYOffset + 'px';
					}										
				}
			}
		}
	}


	/*
	This is our mouse out handler to remove the tooltip
	*/
	function dwOnMouseOutMarker( e )
	{
		var from = is_ie ? event.fromElement : e.target;
		
		// check that we are the first marker element to receive the event
		if ( this == dwGetFirstMarkerHighlightOrToolTip( from ) )
		{
			var to = is_ie ? event.toElement : e.relatedTarget;
			
			// if it is a move from the marker into the tooltip or highlight we don't remove the highlighting
			enter = dwGetFirstMarkerHighlightOrToolTip( to );
			if ( enter != dwToolTip && enter != dwActiveHighlight )
			{
				dw.__ux.unhighlight(dwActiveElement);
				dwActiveElement = null;
				//this.style.backgroundColor = dwOldBackground.color;
				//this.style.backgroundImage = dwOldBackground.image;
				dwToolTip.style.visibility = 'hidden';
			}
		}
	}


	/*
	Called when the mouse enters the tooltip.
	*/
	function dwOnMouseOverToolTip( e )
	{
	}


	/*
	This function removes the tooltip when the mouse finally leaves it.
	*/
	function dwOnMouseOutToolTip( e )
	{
		var to = is_ie ? event.toElement : e.relatedTarget;

		// The event can happen with all elements contained in the tooltip. We check here that
		// it is really a move outside the tooltip.
		if ( this != dwGetFirstMarkerHighlightOrToolTip( to ) )
		{
			if ( dwActiveElement != null )
			{
				dw.__ux.unhighlight(dwActiveElement);
				//dwActiveElement.style.backgroundColor = dwOldBackground.color;
				//dwActiveElement.style.backgroundImage = dwOldBackground.image;
				dwActiveElement = null;
			}
			dwToolTip.style.visibility = 'hidden';
		}
	}


	/*
	The method finds the first marker element along the DOM hierachy.
	*/
	function dwGetFirstMarker( element )
	{
		while( element != null && element.onmouseover != dwOnMouseOverMarker )
		{
			element = dwGetParentNode(element);
		}
		return element;
	}


	/*
	The method finds the first marker element or the tooltip along the DOM hierachy.
	*/
	function dwGetFirstMarkerHighlightOrToolTip( element )
	{
		while( element != null && element != dwToolTip && element.onmouseover != dwOnMouseOverMarker && element != dwActiveHighlight)
		{
			element = dwGetParentNode(element);
		}
		return element;
	}

	/*
	This methods looks in image sources and links for the organization name and the site name, since the SEO urls don't provide
	neither the organization name nor the site name in the url, but it's necessary for creating the content of the bookmarklets.
	*/
	function dwGetOrganizationAndSiteName()	{
		 function extractStorefrontSite(url) {
		 	 var start = url.indexOf("/Sites");
		 	 if (start == -1) {
		 		 start = url.indexOf("/Demos");
		 	 }
		 	 
		 	 if (start == -1) {
		 		 return null;
		 	 }
		 	 
		 	 start = start + 1;
		 	 var end = url.indexOf("/", start);
		 	 if (end == -1) {
		 		 return null;
		 	 }
		 	 
		 	 var domain = url.substring(start, end);
		 	 if (domain.indexOf("-") == domain.lastIndexOf("-")) {
		 		 return null;
		 	 }

		 	 return domain;
		 }
		
		 var images = document.images;
		 for (var i = 0; i < images.length; i++) {
		 	if (images[i] != null) {
		 		var domain = extractStorefrontSite(images[i].src);
		 		if (domain != null)	{
		 			return domain; 
		 		}
		 	}
		 }

		 var links = document.links;
		 for (var i = 0; i < links.length; i++) {
			 if (links[i] != null) {
				 var domain = extractStorefrontSite(links[i].href);
				 if (domain != null) {
					 return domain;
				 }
		 	}
		 }

		 return "";
	}
	
	/*
	This methods looks in links for a URL to manipulate into content information edit links, 
	since the window location may be a site alias.
	*/
	function dwGetBaseEditURL()
	{
		var links   = document.links;
		var link = "";
		for( i=0;i<links.length;i++ )
		{
			if( (links[i]!=null) 
					&& (links[i].href != null) 
					&& (links[i].href.indexOf("demandware.store") > 0) 
					&& (links[i].href.length > link.length))
			{
				link = links[i].href;
			}
		 }
		return link;
	}
	
	/** Returns the parent node of the given element, or null. */
	function dwGetParentNode(element)
	{
		try {
			return element.parentNode;
		} 
		catch (e) {
			return null;
		}
	}
	
	// Register content information type
	dw.__ux.registerInformationType({
		id: 'content-information',
		text: 'Content Information',
		show: function() {
			dw.__ux.hideInformationType('page-information');
			showContentInformation();
		},
		hide: dw.__ux.pm.hidePageMarkers
	});
	
	// Register page information type
	dw.__ux.registerInformationType({
		id: 'page-information',
		text: 'Page Information',
		show: function() {
			dw.__ux.hideInformationType('content-information');
			showPageInformation();
		},
		hide: dw.__ux.pm.hidePageMarkers
	});
}());